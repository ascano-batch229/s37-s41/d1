const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseControllers");
const auth = require("../auth.js");

// Create a course
/*router.post("/", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));
})*/

router.post("/", auth.verify, (req, res) => {

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseController.addCourse(data).then(resultFromController => res.send(resultFromController));
});

// Retrieve all courses
router.get("/all", (req, res) => {

	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
});

/*
	Mini-Activity 
	1. Create a route that will retrive ALL ACTIVE courses (endpoint: "/")
	2. No need of user to login
	3. Create a controller that will return ALL ACTIVE courses4
	4. Send your output Postman output screenshot in our batch hangouts


*/

// Solution: Get all active
router.get("/", (req, res) => {
	 courseController.getAllActive().then(resultFromController => res.send(resultFromController));
})

// retrieve a specific course
router.get("/:courseId", (req, res) => {
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
})

// update a course
router.put("/:courseId", auth.verify, (req,res) => {

	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
})

// Archiving a course
router.put("/:courseId/archive", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin == true){
		courseController.archiveCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
	}else{
		res.send("Invalid User");
	}
});



// Exports the router object for index.js file
module.exports = router;