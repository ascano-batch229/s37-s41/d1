const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers.js");
const auth = require("../auth.js");

// Check email if it existing to our database
router.post("/checkEmail", (req,res) => {
	userControllers.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

//User Registration route
router.post("/register", (req,res) => {
	userControllers.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

//User Login
router.post("/login", (req,res) => {
	userControllers.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Retrieving user details
// auth.verify - middle to ensure that the user is logged in before they can retrieve their details.
router.post("/details", auth.verify, (req,res) => {

	// uses the decode method defined in the auth.js to retrieve user info from the request header
	const userData = auth.decode(req.headers.authorization);

	userControllers.getProfile({userId: req.body.id}).then(resultFromController => res.send(resultFromController));
})

// Entroll a user
/*router.post("/enroll", (req,res) => {

	// values needed to enroll, one for the user who will enroll, one for the course they would like to enroll in.
	let data = {
		userId: req.body.userId,
		courseId: req.body.courseId
	}

	userControllers.enroll(data).then(resultFromController => res.send(resultFromController));
})*/

// Logged in user enrollment

router.post("/enroll", auth.verify, (req,res) => {

	// values needed to enroll, one for the user who will enroll, one for the course they would like to enroll in.
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	}

	userControllers.enroll(data).then(resultFromController => res.send(resultFromController));
})




// this is to connect the route to other modules
module.exports = router;