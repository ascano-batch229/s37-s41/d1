const Course = require("../models/Course.js");
const auth = require("../auth.js");
const User = require("../models/User.js");

// Create new course
/*
	Steps:
	1. Create a new Course object using the mongoose model
	2. Save the new Course to the database.

*/

module.exports.addCourse = (data) => {

	if(data.isAdmin){

		// Creates a new variable "newCourse" and instantiate a new Course object
		// Uses the info from the request body to provide all necessary info
		let newCourse = new Course({

			name:reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		});

		//saves the created object to our database
		return newCourse.save().then((course, error) => {
			// course creation failed
			if(error){

				return false;
			// course creation succeed
			}else{
				return true;
			};
		});

	}else{
		return false;

	}
} 


// retrieve all courses

module.exports.getAllCourses = () => {

	return Course.find({}).then(result => {
		return result;
	});
}

//  Retrieve all active courses

module.exports.getAllActive = () => {

	return Course.find({isActive:true}).then(result => {
		return result;
	});
}

// retrieve specific course

module.exports.getCourse = (reqParams) => {
	console.log(reqParams);
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	})
}


// update a course
/*
	Steps:
	1. Create a variable "updatedCourse" which will contain the info retrieved from the reqBody
	2. Find and update the course using the course ID retrieve from the req params and the variable "updatedCourse" 
*/

// info for updating will be coming from URL parameters and request body
module.exports.updateCourse = (reqParams, reqBody) => {

	// specify the fields of the document to be updated
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
		
	};

	// findbyIdAndUpdate(document ID, updatesToBeApplied)
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
		// course not updated
		if(error){
			return false;
		// course updated succesfully 
		}else{
			return true;
		}
	})
} 


//  Archiving a course

module.exports.archiveCourse = (reqParams, reqBody) => {

	let archivedCourse = {
		isActive: reqBody.isActive
	};

	return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((course, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}





